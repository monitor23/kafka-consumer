package com.kafka.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kafka.dto.PayloadDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class MTEventsConsumer {

    public static final String UPLOAD_BATCH = "http://172.31.31.93:8080/uploadBatch";

    @KafkaListener(topics = {"mt-events"})
    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {
        sendEvent(consumerRecord.value());
    }

    private void sendEvent(String value) {
        RestTemplate restTemplate = new RestTemplate();
        PayloadDTO payloadDTO = new PayloadDTO();
        payloadDTO.setContent(value);
        try {
                restTemplate.postForObject(UPLOAD_BATCH, payloadDTO, PayloadDTO.class);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
        }

    }
}
