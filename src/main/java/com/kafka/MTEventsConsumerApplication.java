package com.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MTEventsConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MTEventsConsumerApplication.class, args);
	}

}
